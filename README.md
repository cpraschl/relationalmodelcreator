# RelationalModelCreator

Little HTML page with javascript for creating a textual notated relational model, with the possibility to export/import it as/from a .json file.

![screenshot](./images/screenshot.jpg)

## Get started

There are two ways to use the tool:

- [Use the online hosted (via GitLab Pages) version here](https://cpraschl.gitlab.io/relationalmodelcreator/relationalmodeltool.html)
- Clone the repository and open the `relationalmodeltool.html` or just download the file itself and use it

## How to

If you have got a valid .json representation you can use the load button to import the relations.

**Note:** If you use the load option with an already created model, already existing relations are __not__ updated, but missing relations are added! 

If you want to create a new relational model execute the following steps:

1. Enter a name for the relation
2. Press the "Add relation" button
3. Select the created relation
4. Enter a name for an attribute, which should be added to the selected relation
5. (optional) Select a data type
6. (optional) Check if the attribute is a primary key
7. (optional) Check if the attribute is a foreign key
8. Press the "Add attribute" button
9. Repeat steps 4 to 8 until all required attributes were added
10. Repeat steps 1 to 9 until you have created all the required relations
11. Press the save button (Strg + Alt + S) for creating a .json representation of the relational model

If you made a mistake you can:

- Update the relation/attribute by selecting it, changing the wrong value and press the 'Update' button
- Move the relation/attribute in the view using the arrow buttons or arrow keys
- Delete the relation/attribute using the delete button

## Json Model

The used json model consists of a root node with a single array property called "relations".
Every relation element in this array consists of two properties:

- name: Name of the relation
- attributes: Array containing objects representing a single attribute

An attribute is defined by the following properties:

- name: Name of the attribute
- type: Type of the attribute (None, String, Number, Datetime, Boolean)
- isPrimary: Boolean flag signaling if the attribute is a primary key
- isForeign: Boolean flag signaling if the attribute is a foreign key

Valid Example with 3 relations and multiple attributes:

```json
{
   "relations":[
      {
         "name":"rel1",
         "attributes":[
            {
               "name":"a1",
               "type":"String",
               "isPrimary":true,
               "isForeign":false
            },
            {
               "name":"a2",
               "type":"Number",
               "isPrimary":false,
               "isForeign":false
            },
            {
               "name":"a3",
               "type":"None",
               "isPrimary":true,
               "isForeign":true
            },
            {
               "name":"a4",
               "type":"Boolean",
               "isPrimary":false,
               "isForeign":true
            }
         ]
      },
      {
         "name":"rel2",
         "attributes":[
            {
               "name":"a1",
               "type":"None",
               "isPrimary":false,
               "isForeign":false
            },
            {
               "name":"a2",
               "type":"None",
               "isPrimary":false,
               "isForeign":false
            },
            {
               "name":"a3",
               "type":"None",
               "isPrimary":false,
               "isForeign":false
            }
         ]
      },
      {
         "name":"rel3",
         "attributes":[
            {
               "name":"a0",
               "type":"None",
               "isPrimary":false,
               "isForeign":false
            }
         ]
      }
   ]
}
```

## Technology

This application was created with plain HTML and JavaScript without any additional frameworks.
Extracted CSS rules of Bootstrap 4.4.1 are used for a basic styling. So it should work in all modern browsers.

## Bug report

This little application was created with the best to my knowledge, but errors are never excluded.
If you find a bug please report it with the following information:

- What bug did you find?
- What have you done so far until the bug occured?
- Which version did you use of the relation model creator (version number in header or even better commit number)?
- Which browser (type + version) did you use?
- Add screenshots showing the error.
- Add screenshots of the javascript developer console if possible.
